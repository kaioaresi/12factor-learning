# Build, release, run

#### Separe estritamente os estágios de *build* e *run*

Uma base de código é transformada num deploy (de não-desenvolvimento) através de três estágios:

- O estágio de *build* é uma transformação que converte um repositório de código em um pacote executável conhecido como *build*. Usando uma versão do código de um commit especificado pelo processo de desenvolvimento, o estágio de *build* obtém e fornece dependências e compila binários e ativos.
- O estágio de *release* pega a *build* produzida pelo estágio de *build* e a combina com a atual configuração do deploy. O *release* resultante contém tanto a *build* quanto a configuração e está pronta para *run* imediata no ambiente de *run*.
- O estágio de *run* roda o app no ambiente de *run*, através do início de alguns dos processos do app com um determinado `release`.

O app doze-fatores usa separação estrita entre os estágios de `build`, `release` e `run`. Por exemplo, é impossível alterar código em tempo de `run`, já que não há meios de se propagar tais mudanças de volta ao estágio de `build`.

![Build Run Release](img/release.png "Build Run Release")

Ferramentas para deploy tipicamente oferecem ferramentas de gestão de lançamento, mais notadamente a habilidade de se reverter à um lançamento prévio. Por exemplo, a ferramenta de deploy Capistrano armazena lançamentos em um subdiretório chamado `releases`, onde o lançamento atual é um link simbólico para o diretório de lançamento atual. Seu comando `rollback` torna fácil reverter para um lançamento prévio.

Cada lançamento deve sempre ter um identificador de lançamento único, tal qual o timestamp do lançamento (como `2011-04-06-20:32:17`) ou um número incremental (como `v100`). Lançamentos são livro-razões onde apenas se acrescenta informações, ou seja, uma vez criado o lançamento não pode ser alterado. Qualquer mudança deve gerar um novo lançamento.

Builds são iniciadas pelos desenvolvedores do app sempre que novos códigos entram no deploy. A execução de um executável, todavia, pode acontecer automaticamente em casos como o reinício do servidor, ou um processo travado sendo reiniciado pelo gerenciador de processos. Assim, no estágio de execução deve haver quanto menos partes móveis quanto possível, já que problemas que previnem um app de rodar pode causá-lo a travar no meio da noite quando não há desenvolvedores por perto. O estágio de construção pode ser mais complexo, já que os erros estão sempre à vista do desenvolvedor que está cuidando do deploy.
