# 12 Factor

## Introdução

Na era moderna, software é comumente entregue como um serviço: denominados web apps, ou software-como-serviço. A aplicação doze-fatores é uma metodologia para construir softwares-como-serviço que:

Usam formatos declarativos para automatizar a configuração inicial, minimizar tempo e custo para novos desenvolvedores participarem do projeto;
Tem um contrato claro com o sistema operacional que o suporta, oferecendo portabilidade máxima entre ambientes que o executem;
São adequados para implantação em modernas plataformas em nuvem, evitando a necessidade por servidores e administração do sistema;
Minimizam a divergência entre desenvolvimento e produção, permitindo a implantação contínua para máxima agilidade;
E podem escalar sem significativas mudanças em ferramentas, arquiteturas, ou práticas de desenvolvimento.
A metodologia doze-fatores pode ser aplicada a aplicações escritas em qualquer linguagem de programação, e que utilizem qualquer combinação de serviços de suportes (banco de dados, filas, cache de memória, etc).


---
# Referências

- [12 factor](https://12factor.net/pt_br/)
- [Patterns of Enterprise Application Architecture](https://books.google.com.br/books/about/Patterns_of_enterprise_application_archi.html?id=FyWZt5DdvFkC&redir_esc=y&hl=pt-BR)
- [Refactoring](https://books.google.com.br/books/about/Refactoring.html?id=1MsETFPD3I0C&redir_esc=y&hl=pt-BR)
- [Capistrano](https://github.com/capistrano/capistrano)
- [Tornado](https://www.tornadoweb.org/en/stable/)
- [5 Years of 12 Factor Apps by Joe Kutner](https://www.youtube.com/watch?v=jufe_sHejXc&ab_channel=Devoxx)
