# Port binding

#### Exporte serviços via port binding

Apps web as vezes são executadas dentro de container de servidor web. Por exemplo, apps PHP podem rodar como um módulo dentro do Apache HTTPD, ou apps Java podem rodar dentro do Tomcat.

O aplicativo doze-fatores é completamente `self-contained` e não depende de injeções de tempo de execução de um servidor web em um ambiente de execução para criar um serviço que defronte a web. O app web exporta o HTTP como um serviço através da binding to a port, e escuta as requisições que chegam na mesma.

Num ambiente de desenvolvimento local, o desenvolvedor visita a URL de um serviço como http://localhost:5000/ para acessar o serviço exportado pelo seu app. Num deploy, uma camada de roteamento manipula as requisições de rotas vindas de um hostname público para os processos web atrelados às portas.

Isso é tipicamente implementado usando declaração de dependências para adicionar uma biblioteca de servidor ao app, tal como Tornado para Python, Thin para Ruby, ou Jetty para Java e outra linguagens baseadas na JVM. Isso acontece completamente no espaço do usuário, isso é, dentro do código do app. O contrato com o ambiente de execução é binding a uma porta para servir requisições.

HTTP não é o único serviço que pode ser exportado via binding port. Quase todos os tipos de software servidores podem rodar via um processo vinculado a uma porta e aguardar as requisições chegar. Exemplos incluem ejabberd (comunicando via XMPP), e Redis (comunicando via protocolo Redis).

Note que a abordagem de binding portas significa que um app pode se tornar o serviço de apoio para um outro app, provendo a URL do app de apoio como um identificador de recurso na configuração para o app consumidor.
